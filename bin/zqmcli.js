#!/usr/bin/env node
'use strict';
const { green } = require('../utils/chalk');
const { question } = require('../utils/question');
const program = require('commander');
const inquirer = require('inquirer');
const create = require('../src/create');
const fs = require('fs');

 
/* zqmcli create 创建项目 */
program.command('create').description('create a project').action(function() {
    green('欢迎使用zqmcli, 轻松构建应用前端项目!')
    inquirer.prompt(question).then(answer=>{
        console.log('answer', answer);
        let { conf, name } = answer
        if(conf){
            /* 创建文件 */
            fs.mkdir(name, (err) => { if(err) throw err });
            create(answer)
        }
    })
})

/* zqmcli init 初始化项目 */ 
program.command('init').description('init a project').action(function(){
    green('-----初始化项目-----')
})

/* zqmcli start 运行项目 */
program.command('start').description('start a project').action(function(){
    green('-----运行项目-----')
})

/* zqmcli build 打包项目 */
program.command('build').description('build a project').action(function(){
    green('-----构建项目-----')
})

program.parse(process.argv)