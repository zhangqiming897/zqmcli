const path = require("path");
// 压缩和清理代码
const TerserWebpackPlugin = require("terser-webpack-plugin");

module.exports = {
    stats: "errors-only",
    optimization: {
        minimize: true,
        minimizer: [
            new TerserWebpackPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true, // 删除console
                        drop_debugger: true, // 删除debugger
                    },
                    output: {
                        comments: false, // 去除代码注释
                    }
                },
                extractComments: false
            })
        ]
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, '../src'), // 路径映射
        },
        extensions: [".js", ".jsx", ".ts", ".tsx"]
    },
    output: {
        filename: "[name].[contenthash:4].js",
        clean: true, // 清理dist文件夹
    },
    module: {

    },
    plugins: []
}
