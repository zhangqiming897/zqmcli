const path = require('path');
// 合并
const { merge } = require("webpack-merge");
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const comConfig = require("./webpack.common");

const devConfig = {
    mode: 'development',
    entry: path.join(__dirname, '../src/app'),
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, '../dist/development'),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react'
                        ],
                    },
                },
                exclude: '/node_modules/',
            },
            // 使用ts
            {
                test: /\.(ts|tsx)$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(css|less)$/,
                exclude: /\.module\.less$/,
                use: [
                    'style-loader',
                    { 
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                        } 
                    },
                    'less-loader'
                ],
            },
            {
                test: /\.module\.less$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: true,
                        }
                    },
                    'less-loader'
                ]
            },
            // 在node_modules 中的css, 不开启
            {
                test: /\.css$/,
                include: /node_modules/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
        ]
    },
    plugins: [
        new webpack.IgnorePlugin({
            resourceRegExp: /\.d\.ts$/
        }),
        new htmlWebpackPlugin({
            template: path.resolve(__dirname, '../index.html'),
            filename: 'index.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[hash].css',
        }),
    ],
    devServer: {
        open: true,
        hot: true
    }
}

module.exports = merge(devConfig, comConfig);