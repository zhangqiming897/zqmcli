const path = require("path");
// 合并
const { merge } = require("webpack-merge");
// 导入每次删除文件夹的插件
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
// 压缩包体积
const nodeExternals = require("webpack-node-externals");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const comConfig = require("./webpack.common");

const proConfig = {
  mode: "production",
  entry: path.join(__dirname, "../src/app"),
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "../dist/production"),
    libraryTarget: "umd",
    library: "ReactCmp",
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
          },
        },
        exclude: "/node_modules/",
      },
      {
        test: /(\.tsx|.ts)$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /(\.css|.less)$/,
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader",
            options: {
              modules: true,
            },
          },
          { loader: "less-loader" },
        ],
      },
      {
        test: /\.css$/,
        include: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
  externals: [nodeExternals()],
  plugins: [
    // 插件
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
    }),
  ],
};

module.exports = merge(proConfig, comConfig);