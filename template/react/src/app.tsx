import React, { ReactElement, Suspense } from "react";
import { HashRouter as Router, Routes, Route } from "react-router-dom";
import { ConfigProvider } from "antd";
import ReactDOM from "react-dom";
import { routes } from "@/router/config";

// 深层次嵌套路由
const levelTreeRoutes = (routes: any) => {
  return routes.map((route: any, index: number) => {
    const { path, children } = route;
    return (
      <Route key={index} path={path} element={<route.element />}>
        {
          children ? levelTreeRoutes(children) : null
        }
      </Route>
    )
 })
}

const Main = (): ReactElement => {
  return (
    <ConfigProvider>
      <Suspense>
        <Router>
          <Routes>
            {levelTreeRoutes(routes)}
          </Routes>
        </Router>
      </Suspense>
    </ConfigProvider>
  );
};

ReactDOM.render(<Main />, document.getElementById("app"));
