import React, { useEffect, useState } from "react";
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import {
  Layout,
  Menu,
} from "antd";
import { adminRoutes, routes } from "../router/config"
import styles from "@/assets/common/layout.module.less";

const { Header, Content, Sider } = Layout;

const Admin = () => {
    const navigate = useNavigate();
    const pathname = useLocation().pathname;
    
    const [key, setKey] = useState(pathname);

    useEffect(() => {
      if(pathname == '/') findCurrentKey(routes)
    }, [])

    // 遍历找寻当前key值
    const findCurrentKey = (routes: any) => {
      let convertRoutes = routes[0].children;
      if(convertRoutes.length) {
        for(let i=0; i<convertRoutes.length; i++) {
           let item = convertRoutes[i];
           if(item.children) {
              findCurrentKey(item.children);
           } else {
              setKey(item.path);
              navigate(item.path);
              break;
           }
        }
    }
   }
    // 嵌套路由转菜单权限树
    const convertMenusTree = (routes: any) => {
      let convertRoutes = routes[0].children;
      if(convertRoutes.length) {
          return convertRoutes.map((route: any) => {
              const { path, children, meta } = route;
              return {
                  key: path,
                  label: meta.pageTitle,
                  path: path,
                  children: children ? convertMenusTree(children) : undefined
              }
          })
      }
    }

    // 获取路由菜单
    const routeMenus = convertMenusTree(adminRoutes);

    const selectMenu = (item: any) => {
       const path = (routeMenus.find((menu: any) => menu.key == item.key) as any)?.path;
       path && navigate(path);
       setKey(path);
    }
    return (
        <div className={styles["container"]}>
            <Layout className={styles["container-layout"]}>
                <Sider className={styles["container-layout_sider"]}>
                  <div className={styles["sider-head"]}>
                    
                  </div>
                  <div className={styles["sider-menu"]}>
                    <Menu
                      theme="light"
                      mode="vertical"
                      selectedKeys={[key]}
                      items={routeMenus}
                      onSelect={selectMenu}
                    />
                  </div>
                </Sider>
                <Layout>
                    <Header className={styles["container-layout_header"]}></Header>
                    <Content className={styles["container-layout_content"]}>
                        <div className={styles["content-container"]}>
                          <Outlet />
                        </div>
                    </Content>
                </Layout>
            </Layout>
        </div>
      )
}

export default Admin;