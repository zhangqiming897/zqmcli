import { lazy } from 'react';
const AdminView = lazy(() => import("@/layouts/admin"));
const DashboardPage = lazy(() => import("@/layouts/pages/dashboard"));
const HomePage = lazy(() => import("@/layouts/pages/home"));

interface IMeta{
    pageTitle: string
}
interface IRouter {
    path: string;
    element: React.LazyExoticComponent<() => JSX.Element>;
    meta?: IMeta;
    children?: IRouter[];
}

// 容器路由
const adminRoutes: IRouter[] = [
    {
        path: "/",
        element: AdminView,
        meta: { pageTitle: '管理' },
        children: [
            {
                path: "/home",
                element: HomePage,
                meta: { pageTitle: '首页' },
            }, 
            {
                path: "/dashboard",
                element: DashboardPage,
                meta: { pageTitle: '面板' }
            }
        ]
    }
]

const routes: IRouter[] = [
    ...adminRoutes
]

export {
    routes,
    adminRoutes
};