/**
 * 时间换算
 */
const showTimeFormat = (formatStr: string, fdate: string) => {
  const addZero = (num: number) => {
    return num < 10 ? "0" + num : num;
  };
  let fTime,
    fStr = "ymdhis";
  if (!formatStr) formatStr = "y-m-d h:i:s";
  if (fdate) {
    fTime = new Date(fdate);
  } else {
    fTime = new Date();
  }
  let formatArr = [
    fTime.getFullYear().toString(),
    addZero(fTime.getMonth() + 1).toString(),
    addZero(fTime.getDate()).toString(),
    addZero(fTime.getHours()).toString(),
    addZero(fTime.getMinutes()).toString(),
    addZero(fTime.getSeconds()).toString(),
  ];
  for (let i = 0; i < formatArr.length; i++) {
    formatStr = formatStr.replace(fStr.charAt(i), formatArr[i]);
  }
  return formatStr;
};

/**
 * 计算月份差
 */
const datemonth = (date1: any, date2: any) => {
  // 拆分年月日
  date1 = date1.split("-");
  // 得到月数
  date1 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
  // 拆分年月日
  date2 = date2.split("-");
  // 得到月数
  date2 = parseInt(date2[0]) * 12 + parseInt(date2[1]);
  var m = Math.abs(date1 - date2); //Math.abs()取绝对值
  return m;
};

/**
 *  判断是否是json
 * */
const jsJson = (str: string) => {
  if (typeof str == "string") {
    try {
      let obj = JSON.parse(str);
      if (typeof obj == "object") {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
};

/**
 * json深层次遍历解析
 *  */
const analysisJson = (obj: Object) => {
  for (let key in obj) {
    if (jsJson(obj[key])) {
      obj[key] = JSON.parse(obj[key]);
      analysisJson(obj[key]);
    }
  }
  return obj;
};

export {
    showTimeFormat,
    datemonth,
    jsJson,
    analysisJson
}