import { PropType, defineComponent, reactive, toRefs, watch } from "vue";
import { UploadChangeParam, UploadFile, UploadProps, message, Upload } from "ant-design-vue";
import { UploadOutlined } from "@ant-design/icons-vue";
import styles from "./index.module.less";

/**
 * 配置项设置
 */

interface ExtraPropertyType {
    limit?: number
}

export default defineComponent({
    props: {
       uploadConfigs: {
          type: Object as PropType<UploadProps & ExtraPropertyType>,
          required: true
       }
    },
    components: {
        aUpload: Upload
    },
    setup(props, { slots, emit }) {
        // toRefs解构保持响应式
        let { uploadConfigs } = toRefs(props);
        let { fileList, limit } = uploadConfigs.value;
        let exposeUploadProps = reactive({ fileList })

        /**
         * 文件上传前处理
         */
        const handleBeofreUpload = (file: UploadFile) => {
            const fileMax = limit ? limit : 1024 * 1024 * 2;
            return new Promise((resolve, reject) => {
                 let fileSize = file.size as unknown as number;
                 if(fileSize > fileMax) {
                    message.error(`图片最大不能超过${fileMax /( 1024 * 1024 )}M`);
                    reject(false);
                 } else {
                    resolve(true);
                 }
            })
        }

        /**
         * 上传状态改变时回调
         */
        const handleChangeUpload = (event: UploadChangeParam) => {
             switch(event.file.status) {
                 case 'done':
                    emit("success", { file: event.file })
                 break;
                 case 'error':
                    try {
                        event.fileList.forEach((item: UploadFile, index: number) => {
                            if(Object.is(item.uid, event.file.uid)) {
                                event.fileList.splice(index, 1)
                                throw new Error('执行结束')
                            }
                        })   
                    } catch(e) {
                        throw e
                    }
                 break;
                 default:break;
             }
        }

        /**
         * 监听输入框值改变
         */
        watch(() => uploadConfigs.value, (next, prev) => {
            let { fileList } = next
            exposeUploadProps.fileList = fileList
        })

        return () => (
            <div class={styles["upload-main"]}>
                <a-upload
                   {...Object.assign({}, uploadConfigs, 
                     { 
                        fileList: exposeUploadProps.fileList,
                        beforeUpload: handleBeofreUpload          
                    })
                   } onChange={handleChangeUpload}>
                    <a-button>
                        <UploadOutlined />
                        上传图片
                    </a-button>
                </a-upload>
                {slots}
            </div>
        )
    }
})