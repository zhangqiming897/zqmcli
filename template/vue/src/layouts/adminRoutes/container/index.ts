export { default as Sider } from './sider.vue';
export { default as Header } from './header.vue';
export { default as Content } from './content.vue';
