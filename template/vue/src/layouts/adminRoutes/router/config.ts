export const adminAuthority = [
  {
    level: 0,
    path: "/",
    name: "manage",
    component: () => import("@/layouts/adminRoutes/layout/manage.vue"),
    children: [
      {
        level: 1,
        path: "/test",
        name: "test",
        title: "测试",
        component: () => import("@/layouts/adminRoutes/pages/index.vue"),
      },
    ],
  },
];
