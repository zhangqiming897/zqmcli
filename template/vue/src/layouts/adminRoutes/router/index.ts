import { createRouter, createWebHashHistory } from 'vue-router';
import { adminAuthority } from './config';

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        ...adminAuthority
    ]
})