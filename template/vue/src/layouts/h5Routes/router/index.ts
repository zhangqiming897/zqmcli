import { createRouter, createWebHashHistory } from 'vue-router';
import { h5Authority } from './config';

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        ...h5Authority
    ]
})