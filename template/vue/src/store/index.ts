/**
 * 全局状态管理
 */
import { ref } from "vue";
import { createStore } from "vuex";
import { login } from "./modules";

export default createStore({
  state: {
    collapsed: ref<Boolean>(false),
    openKeys: ref<Array<string>>([]),
    selectedKeys: ref<Array<string>>([]),
  },
  mutations: {
    // 更改数据状态
    updateCollapse(state, payload) {
      state.collapsed = payload.collapsed;
    },

    // 更改侧边栏的选中状态
    updateSelectedKeys(state, payload) {
      state.selectedKeys = payload.selectedKeys;
    },

    // 更改侧边栏二级菜单的打开状态
    updateOpenKeys(state, payload) {
      state.openKeys = payload.openKeys;
    },
  },
  actions: {
    // 更改数据状态
    asyncUpdateCollapse({ commit }, payload) {
      commit("updateCollapse", payload);
    },

    // 更改侧边栏的选中状态
    asyncUpdateSelectedKeys({ commit }, payload) {
      commit("updateSelectedKeys", payload);
    },

    // 更改侧边栏的二级菜单的打开状态
    asyncUpdateOpenKeys({ commit }, payload) {
      commit("updateOpenKeys", payload);
    },
  },
  getters: {
    collapsed: (state) => state.collapsed,
  },
  // modules
  modules: {
    login,
  },
});
