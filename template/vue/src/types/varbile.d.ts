declare enum resposeCode {
    failedCode = 1, // 失败
    successCode = 0, // 成功
    serverErrorCode = -1, // 服务端错误
    noVoidToken = 102 // 没有token
}

interface resposeData<T = unknown> {
    code: resposeCode;
    data: T;
    message: string;
    success: boolean | string;
    [par: string]: any;
}

interface requestData<T = unknown> {
    baseURL?: string;
    url: string;
    data?: T;
    params?: T;
    method?: string;
    headers?: T;
    timeout?: number;
    responseType?: T
}

interface pendingType<T = unknown> {
   url?: string;
   method?: string;
   params: T;
   data: T;
   cancel: Function
}

declare module 'js-md5';