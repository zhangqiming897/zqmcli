/**
 * 设计模式: 适配器模式
 * 描述: 通过适配器处理if-else逻辑判定
 */

class Adpater {
  data = [];
  constructor(data: any) {
    this.data = data;
  }

  // 列表模式
  toList(col: any) {
    return this.data.map((v: any) => {
      const obj = {} as any;
      for (let e of col) {
        const f = e.f;
        obj[f] = v[f];
      }
      return obj;
    });
  }

  // 单选模式
  toSelect(opt: any) {
    const { label, value } = opt;
    return this.data.map((v: any) => ({ label, value }));
  }

  // 复选框模式
  toChecked(opt: any) {
    const { field } = opt;
    return this.data.map((v: any) => ({ checked: false, value: v[field] }));
  }
}

export default Adpater;
