import axios, { AxiosRequestConfig, AxiosRequestHeaders } from "axios";

// 取消重复请求
const pending: Array<pendingType> = [];
// 中断请求 --- 类似原生的abort()
const CancelToken = axios.CancelToken;

// axios 实例
const instance = axios.create({
  timeout: 10000,
  responseType: "json",
});

// 移除重复请求
const removePending = (config: AxiosRequestConfig) => {
  pending.forEach((item, index) => {
    const count: number = +index;
    // 执行存在请求体的函数
    if (
      item.url === config.url &&
      item.method === config.method &&
      JSON.stringify(item.params) === JSON.stringify(config.params) &&
      JSON.stringify(item.data) === JSON.stringify(config.data)
    ) {
      // 执行取消操作
      item.cancel("操作太频繁，请稍后再试!");
      // 从数组中移除记录
      pending.splice(count, 1);
    }
  });
};

// 添加请求拦截器
instance.interceptors.request.use(
  (config) => {
    removePending(config);
    config.cancelToken = new CancelToken((c) => {
      let { url, method, params, data } = config;
      pending.push({ url, method, params, data, cancel: c });
    });
    let token = sessionStorage.getItem('token');
    if(token) {
      let headers = config.headers as AxiosRequestHeaders;
      headers.AccessToken = token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// 添加响应拦截器
instance.interceptors.response.use(
  (response) => {
    const res = response as unknown as resposeData;
    const code = res.code;
    switch (code) {
      case 102:
        // 根据code, 对业务异常处理(和后端约定)
        break;
      default:
        break;
    }

    return response;
  },
  (error) => {
    const response = error.response;
    // 根据返回的http状态码做不同处理
    switch (response?.status) {
      case 401:
        // token失败
        break;
      case 403:
        // 没有权限
        break;
      case 500:
        // 服务端错误
        break;
      case 503:
        // 服务端错误
        break;
      default:
        break;
    }

    return Promise.reject(error);
  }
);

/**
 * axios 基础构建
 */

class Request {

  // 外部传入的baseUrl
  protected baseURL: string = '/api';

  // 自定义header头
  protected headers: object = {
    ContentType: "application/json;charset=UTF-8",
  };

  private apiAxios({
    baseURL = this.baseURL,
    headers = this.headers,
    method,
    url,
    data,
    params,
    responseType,
  }: requestData): Promise<resposeData> {
    return new Promise((resolve, reject) => {
      instance({
        baseURL,
        headers,
        method,
        url,
        params,
        data,
        responseType,
      } as AxiosRequestConfig)
        .then((res) => {
          resolve(res as unknown as resposeData);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * GET类型的网络请求
   */
  protected getReq({
    baseURL,
    headers,
    url,
    data,
    params,
    responseType,
  }: requestData) {
    return this.apiAxios({
      baseURL,
      headers,
      method: "GET",
      url,
      data,
      params,
      responseType,
    });
  }

  /**
   * POST类型的网络请求
   */
  protected postReq({
    baseURL,
    headers,
    url,
    data,
    params,
    responseType,
  }: requestData) {
    return this.apiAxios({
      baseURL,
      headers,
      method: "POST",
      url,
      data,
      params,
      responseType,
    });
  }
}

export default Request;
