import { resolve } from "path";
import { svgModule } from "./config/svgModule";
import { subModule, getPublicDir } from "./config/subModule";
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import vuesetupextend from "vite-plugin-vue-setup-extend";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { createStyleImportPlugin, VantResolve } from "vite-plugin-style-import";
import Vitecomponents, { AntDesignVueResolver } from "vite-plugin-components";

export default defineConfig(({ mode }) => {
  return {
    plugins: [
      vue(),
      vuesetupextend(),
      vueJsx(),
      Vitecomponents({
        customComponentResolvers: [
          AntDesignVueResolver({
            importLess: true,
            importStyle: "less",
          }),
        ],
      }),
      // 2.0.0 之后版本按需引入
      createStyleImportPlugin({
        libs: [
          {
            libraryName: "ant-design-vue",
            esModule: true,
            resolveStyle: (name) => {
              return resolve(
                __dirname,
                `node_modules/ant-design-vue/es/${name}/style/index`
              );
            },
          },
        ],
        resolves: [VantResolve()],
      }),
      svgModule("./src/assets/svg/"),
    ],
    root: subModule("root", "src/layouts", mode, __dirname),
    publicDir: getPublicDir(mode, __dirname),

    base: "./",
    css: {
      //* css 模块化
      modules: {
        generateScopedName: "[name]_[local]__[hash:base64:5]",
        hashPrefix: "prefix",
      },

      // 配置主题
      preprocessorOptions: {
        less: {
          javascriptEnabled: true,
          ...subModule("theme", "src/layouts", mode, __dirname),
        },
      },
    },
    resolve: {
      alias: {
        "@": resolve(__dirname, "src"),
      },
      extensions: [".js", ".json", ".ts", ".tsx", ".jsx"],
    },
    // 代理服务器
    server: {
      host: true,
      port: 8000,
      open: true, // 热启动
      proxy: {
        "/api": {
          target: loadEnv(mode, process.cwd()).VITE_RES_URL,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
      },
      fs: { strict: false },
    },
    build: subModule("build", "src/layouts", mode, __dirname),
  };
});
